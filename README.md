<p align="center"><a href="https://twitter.com/615_bot" target="_blank" rel="noopener noreferrer"><img width="175" src="https://github.com/crawfordleeds/615_bot/blob/master/assets/logo.jpeg?raw=true" alt="Logo"></a></p>

<p align="center">
    <a href="https://twitter.com/intent/follow?screen_name=615_bot" target="_blank" rel="noopener noreferrer"><img alt="Twitter Follow" src="https://img.shields.io/twitter/follow/615_bot?style=social"></a>
</p>


# 6.15 Twitter Bot

Twitter bot that tweets out the price of 6.15 BTC at 6:15 PM U.S. Mountain Time each day.

## Hosting

This app runs on Heroku using the free tier. Daily tasks are executed using APScheduler and [Kaffeine](https://kaffeine.herokuapp.com/) 
to keep the app "awake".