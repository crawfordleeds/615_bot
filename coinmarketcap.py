import os
import json
import requests
from dotenv import load_dotenv, find_dotenv
ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)


BASE_URL = "https://pro-api.coinmarketcap.com"
COINMARKETCAP_API_KEY = os.environ.get("COINMARKETCAP_API_KEY")

headers = {
    "Accepts": "application/json",
    "X-CMC_PRO_API_KEY": COINMARKETCAP_API_KEY
}
def get_usd_to_btc():
    resp = requests.get(f"{BASE_URL}/v1/cryptocurrency/quotes/latest?id=1", headers=headers)
    print("CMC response:")
    print(json.dumps(resp.json()))

    exchange_rate = resp.json()["data"]["1"]["quote"]["USD"]["price"]
    print("USD to BTC exchange rate: ", exchange_rate)
    return exchange_rate