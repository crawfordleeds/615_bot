import os

from dotenv import load_dotenv, find_dotenv
import tweepy

ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)
def tweet_615(price: float) -> None:
    """
    :param price: the price of 1 BTC
    :return: None
    """
    auth = tweepy.OAuthHandler(os.environ.get("TWITTER_CONSUMER_KEY"), os.environ.get("TWITTER_CONSUMER_SECRET"))
    auth.set_access_token(os.environ.get("TWITTER_ACCESS_TOKEN"), os.environ.get("TWITTER_ACCESS_TOKEN_SECRET"))

    # Instantiate an API object
    api = tweepy.API(auth)

    api.update_status(f"The price of 6.15 BTC is now ${(price * 6.15):,.2f}")
