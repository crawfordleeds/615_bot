from apscheduler.schedulers.blocking import BlockingScheduler
# reference here: https://medium.com/@sagarmanohar/using-apscheduler-for-cron-jobs-on-heroku-26fc8cba525a

from coinmarketcap import get_usd_to_btc
from twitter import tweet_615

sched = BlockingScheduler()


# @sched.scheduled_job('interval', seconds=10)
# @sched.scheduled_job("cron", hour="22", minute="44", timezone="America/Denver")
@sched.scheduled_job("cron", hour="18", minute="15", timezone="America/Denver")
def execute_tweet():
    usd_to_btc = get_usd_to_btc()
    print("usd to btc: ", usd_to_btc)
    tweet_615(usd_to_btc)


sched.start()
